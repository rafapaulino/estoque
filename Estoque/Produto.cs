﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estoque
{
    public class Produto
    {
        private String nomeProduto;
        private double preco;

        public Produto()
        {
            Console.WriteLine("Iniciou o Construtor da Classe");
        }

        ~Produto()
        {
            Console.WriteLine("Chamando o Destrutor da Classe");
        }

        public void setNome(string nome)
        {
            this.nomeProduto = nome;
        }

        public string getNome()
        {
            return this.nomeProduto;
        }

        public void setPreco(double preco)
        {
            this.preco = preco;
        }

        public double getPreco()
        {
            return this.preco;
        }


    }
}
