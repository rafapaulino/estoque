﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estoque
{
    class Program
    {
        static void Main(string[] args)
        {
            Produto produto = new Produto();
            produto.setNome("Melancia");
            produto.setPreco(10.50);

            Console.WriteLine("Produto = " + produto.getNome());
            Console.ReadKey();
        }
    }
}
